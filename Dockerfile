FROM ubuntu:24.04
LABEL maintainer="jonathanpiron@gmail.com"

# Add sab-addons repository for par2-turbo.
RUN apt-get update \
    && apt-get install -y gnupg2 \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 98703123E0F52B2BE16D586EF13930B14BB9F05F
RUN echo deb http://ppa.launchpad.net/jcfp/sab-addons/ubuntu noble main >> /etc/apt/sources.list\
    && echo deb-src http://ppa.launchpad.net/jcfp/sab-addons/ubuntu noble main >> /etc/apt/sources.list

# Install Sabnzbd requirements.
RUN apt-get update && apt-get install -y --no-install-recommends \
        ca-certificates\
        git-core\
        python3\
        openssl\
        unrar\
        unzip\
        p7zip-full\
        par2-turbo\
        python3-pip \
    && rm -rf /var/lib/apt/lists/*

# Set the locale
RUN apt-get update && apt-get install -y --no-install-recommends \
    locales
RUN locale-gen --purge en_US.UTF-8
ENV LANG en_US.utf8
ENV LANGUAGE en_US.utf8
ENV LC_ALL en_US.utf8

# Get Sabnzbd.
ARG VERSION=develop
RUN git clone --depth 1 --branch ${VERSION} https://github.com/sabnzbd/sabnzbd.git /sabnzbd
RUN python3 -m pip install --break-system-packages -r /sabnzbd/requirements.txt

# Create Sabnzbd group and user.
RUN groupadd --system sabnzbd\
    && useradd --system -s /bin/false -d /sabnzbd -g sabnzbd sabnzbd

RUN mkdir /downloads /sabnzbd/datadir\
    && chown -R sabnzbd:sabnzbd /sabnzbd /downloads

VOLUME ["/sabnzbd/datadir", "/downloads"]

EXPOSE 8080

USER sabnzbd

ENTRYPOINT ["/usr/bin/python3", "/sabnzbd/SABnzbd.py", "-s", "0.0.0.0:8080", "-f", "/sabnzbd/datadir", "--disable-file-log"]
