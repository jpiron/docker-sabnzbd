# Docker Sabnzbd

Dockerfile to set up [Sabnzbd](https://sabnzbd.org/)

[![sabnzbd](https://sabnzbd.org/images/logo-full.svg)](https://sabnzbd.org/)

## Repository

Sources are [here](https://gitlab.com/jpiron/docker-sabnzbd).

## Build

The project has git branches per Sabnzbd version. The master branch mirrors the Sabnzbd develop branch.
Each other branch mirrors the respective Sabnzbd version. Tags mirrors Sabnzbd releases.

Prior to build the docker images make sure to checkout the branch/tag corresponding to the version you wish.

The Dockerfile takes an optionnal VERSION build argument corresponding to the wanted Sabnzbd version.
Its default value is `develop`.

```shell
task build VERSION=4.1.0
```

```shell
task build VERSION=4.1.0 -- --no-cache --pull
```

## Run

```bash
docker pull jpiron/sabnzbd
docker run jpiron/sabnzbd
```

However, you're more likely to run the container with following:

```bash
docker run -d --restart=always --name sabnzbd \
-p 8080:8080 \
--user $(id -u ${USER}):$(id -g ${USER}) \
-e TZ=$(cat /etc/timezone) \
-v "${HOME}/.sabnzbd":/sabnzbd/datadir \
-v "${HOME}/downloads":/downloads \
jpiron/sabnzbd
```

## Parameters

* **user**: Docker run parameter to set the user/group to run the container with;
* **TZ**: the timzeone to run the container with;
* **/sabnzbd/datadir**: if you want Sabnzbd data directory to be hold in a local directory. If the directory contains a sabnzbd.ini file it will be used to configure Sabnzbd.
* **/downloads**: provide Sabnzbd with an access to your download directory.

## License

MIT

## Changes

* **20.08.2020**:
  * Update base image to Ubuntu 20.04
  * Update installation for SABnzbd 3+
  * Replace build.sh script by a Makefile
* **22.12.2017**:
  * Update sabyenc version to match 2.3.2 requirements.
* **21.09.2017**:
  * Remove unneeded python-yenc dependency.
  * Update sabyenc version to match 2.3.0 requirements.
* **17.08.2017**: Fix missing locales package.
* **13.03.2017**:
  * Set a proper locale (en_US.utf8).
  * Install Sabnzbd 2.0.0 dependencies.
* **25.01.2017**: Update build.sh behaviour.
* **08.01.2017**: Fix 7zip dependency and remove no longer needed start.sh entrypoint.
* **02.01.2017**: Fix start.sh permissions.
* **04.11.2016**: Remove the PUID/PGID environment variables in favor of the Docker run --user option.
* **02.10.2016**: First release.
